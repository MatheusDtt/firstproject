#to support encodings
import codecs
import os    
from chardet import detect
import sys
import pandas as pd


BLOCKSIZE = 1048576 # or some other, desired size in bytes
# get file encoding type
def get_encoding_type(file):
	
	with codecs.open(file, "rb") as f:
		rawdata = f.read(BLOCKSIZE)  
		return detect(rawdata)['encoding']
	
def convert(file_source, file_target, enconding_source):
	with codecs.open(file_source, "r", enconding_source, errors='ignore') as sourceFile:
		with codecs.open(file_target, "w", "utf-8") as targetFile:
			while True:
				contents = sourceFile.read(BLOCKSIZE)
				if not contents:
					break
				targetFile.write(contents)

	os.remove(file_source) # remove old encoding file
	os.rename(file_target, file_source) # rename new encoding					


directory = os.fsencode(sys.argv[1])				
trg_file = directory.decode() + '\\temp.txt'

for file in os.listdir(directory):
	src_file = os.fsdecode(file)
	complete_path = directory.decode() + '\\' + src_file
	
	if (os.path.isdir(complete_path)):
		continue
	
	src_enconding = get_encoding_type(complete_path)
	print(file.decode() + ' -- ' + src_enconding)
	if (src_enconding != 'utf-8' and src_enconding != 'ascii'):
		convert(directory.decode() + '\\' + src_file, trg_file, src_enconding)
