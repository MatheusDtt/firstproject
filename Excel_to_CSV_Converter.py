#############################################
#### 			Deloitte 				  ###
####		@Autor: Matheus				  ###
#############################################

#to support encodings
import other things 
import codecs
import os    
from chardet import detect
import sys
import pandas as pd
import xlrd
from xlrd import open_workbook, XLRDError

def convert_to_csv(xl_file):
	for sheet in xl_file.sheet_names:
		df = xl_file.parse(sheet)
		df.to_csv(directory.decode() + '\\' + sheet + '.csv', index=False, sep=';')			
		
directory = os.fsencode(sys.argv[1])
df = pd.DataFrame()


for excel in os.listdir(directory):
	file_name = os.fsdecode(excel)
	complete_path = directory.decode() + '\\' + file_name
	
	if (os.path.isdir(complete_path)):
		continue
	try:		
		xl = pd.ExcelFile(complete_path)
	except XLRDError as e:
		print (e)
	else: 
		convert_to_csv(xl)
		os.remove(directory.decode() + '\\' + excel.decode())
		
